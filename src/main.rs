use error_chain::error_chain;
use serde_json::Value;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use tracing::info;
// use tracing_subscriber;

error_chain! {
    foreign_links {
        Io(std::io::Error);
        HttpRequest(reqwest::Error);
    }
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

async fn name(ip: &str) -> Result<String> {
    let settings_url = format!("http://{}/settings", ip);
    let settings_res = reqwest::get(settings_url).await?;
    let settings_body = settings_res.text().await?;
    let settings_val = serde_json::from_str::<Value>(&settings_body).unwrap();
    let device_name = settings_val["name"].as_str().unwrap().trim();
    Ok(device_name.to_string())
}

async fn power(ip: &str) -> Result<f64> {
    let status_url = format!("http://{}/status", ip);
    let status_res = reqwest::get(status_url).await?;
    let status_body = status_res.text().await?;
    let status_val = serde_json::from_str::<Value>(&status_body).unwrap();
    let device_power = &status_val["meters"][0]["power"].as_f64().unwrap();

    Ok(*device_power)
}

async fn state(ip: &str) -> Result<bool> {
    let status_url = format!("http://{}/status", ip);
    let status_res = reqwest::get(status_url).await?;
    let status_body = status_res.text().await?;
    let status_val = serde_json::from_str::<Value>(&status_body).unwrap();

    let device_ison = &if status_val.as_object().unwrap().contains_key("relays") {
        let tmp = status_val.as_object().unwrap();
        &tmp["relays"]
    } else {
        let tmp = status_val.as_object().unwrap();
        &tmp["lights"]
    }[0]["ison"]
        .as_bool()
        .unwrap();

    Ok(*device_ison)
}

async fn info(ip: &str) -> Result<String> {
let device_name = name(ip);
            let device_power = power(ip);
            let device_state = if state(ip).await? { "🟢" } else { "🔴" };
            
            info!("{}", ip);
            Ok(format!("[{}] {} ({}): {}W", device_state, device_name.await?, ip, device_power.await?))
}

#[tokio::main]
async fn main() -> Result<()> {
    // tracing_subscriber::fmt::init();
    if let Ok(lines) = read_lines("./hosts") {
        for ip in lines.flatten().filter(|l| !l.starts_with("//")) {
            println!("{}", info(&ip).await?);
        }
    }
    Ok(())
}
